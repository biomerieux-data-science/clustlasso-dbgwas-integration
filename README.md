# clustlasso-dbgwas-integration

This repository hosts scripts implementing a step-by-step procedure to integrate the [clustlasso](https://gitlab.com/biomerieux-data-science/clustlasso) R package with the [DBGWAS](https://gitlab.com/leoisl/dbgwas) software, as described in the paper [Interpreting k-mer based signatures for antibiotic resistance prediction](https://academic.oup.com/gigascience) (Jaillard *et al.*, 2020, GigaScience).

## Description

The [clustlasso](https://gitlab.com/biomerieux-data-science/clustlasso) package is a generic package implementing both the lasso and cluster-lasso strategies described in the paper [Interpreting k-mer based signatures for antibiotic resistance prediction](https://academic.oup.com/gigascience). While this paper focuses on the task of building predictive models of anti-microbial resistance (AMR) from whole-genome sequences and k-mers, the [clustlasso](https://gitlab.com/biomerieux-data-science/clustlasso) package is generic and can be applied to any other applications, not necessarily involving k-mers nor AMR phenotypes.

This repository provides a step-by-step procedure to integrate [clustlasso](https://gitlab.com/biomerieux-data-science/clustlasso) and the [DBGWAS](https://gitlab.com/leoisl/dbgwas) software, as described in the paper [Interpreting k-mer based signatures for antibiotic resistance prediction](https://academic.oup.com/gigascience) (Jaillard *et al.*, 2020, GigaScience). In short : 
1. **DBGWAS** is first used to build k-mer profiles from a panel of bacterial genomes
2. **clustlasso** is then used to learn sparse predictive models (using either the standard lasso or cluster-lasso stategy), following the general procedure described in the package [vignette](https://gitlab.com/biomerieux-data-science/clustlasso/-/blob/master/vignettes/vignette.pdf)
3. **DBGWAS** is finally used to obtain visualizations associated to the features involved in the model.

## Content

The ressources provided in this repository consist of input data and scripts :
* the input data consist (i) of 50 *Klebsiella pneumoniae* genomes and their associated resistance phenotypes taken from the dataset [[Nguyen, 2018](https://www.nature.com/articles/s41598-017-18972-w)], and (ii) of a fasta file used by DBGWAS to annotate the features selected by the model. They are stored in the file **input.tar.gz**
* the scripts are stored in the **src** directory. They are numbered and must be run one after the other, as described below.

## Usage

After cloning the GitLab repository, you will find (besides this README.md file) : 
* a directory named **src** containing scripts
* a file named **input.tar.gz** containing the required input data, as described above.

The scripts stored in the **script** directory consist of shell and R scripts that must be run one after the other: 

1. The first script is the shell script `00.init-workspace.sh`.
    * it can be run by the command `sh 00.init-workspace.sh`
    * it simply uncompresses the **input.tar.gz** file and creates the **output** directory to store the results of the analysis
2. The second script is the R script `01.run-dbgwas.R`
    * it can be run by the command `Rscript 01.run-dbgwas.R`
    * it creates the input file required by DBGWAS (stored as `output/01.dbgwas-conf.txt`) and launches DBGWAS.
    * note that this process is carried out in this example for the drug meropenem, and that the DBGWAS outputs are stored in the `output/dbgwas` directory.
3. The third script is the script `02.run-clusterlasso.R`
    * it can be run by the command `Rscript 02.run-clusterlasso.R`
    * it runs a standard cross-validation process, as described in the [clustlasso package vignette](https://gitlab.com/biomerieux-data-science/clustlasso/-/blob/master/vignettes/vignette.pdf) and stores the results in the file `output/02.cross-validation-results.Rdata`.
4. The fourth script is the script `03.prepare-input-for-dbgwas-visualization.R`
    * it can be run by the command `Rscript 03.prepare-input-for-dbgwas-visualization.R`
    * it reads the cross-validation results, and generates the plain-text file `output/03.best-models-patterns.txt` to be used by DBGWAS to visualize the features involved in the model selected.
5. The last script is the script `04.get-dbgwas-visualisations.sh`
    * it can be run by the command `sh 04.get-dbgwas-visualisations.sh`
    * it launches DBGWAS a second time, **running the third step only** (i.e., the visualization step only), using as input the `output/03.best-models-patterns.txt` generated above.
    * the visualizations obtained are stored in the directory `output/04.dbgwas-visualizations`. 

**Regarding the visualizations procuced, it is important to note that in order to use the existing DBGWAS visualization utilities, we use the field "q-value" to identify and order the features** (i.e., the clusters) **of the cluster-lasso model**. In other words : the first feature / cluster of the model has a "q-value" set to 1, the second  feature / cluster has a "q-value" set to 2 and so on. **This means in particular that the range of "q-values" considered must be increased from its default value of [0,1] to [0,N] to be able to visualize the top-N features**. The value of the cluster-lasso model coefficients are stored in the fields "Estimated effect" and "Wald statistic".


## Support

Feel free to contact [Magali Jaillard-Dancette](mailto:magali.jaillard-dancette@biomerieux.com) or [Pierre Mahé](mailto:pierre.mahe@biomerieux.com) for further information or to report any issue.



