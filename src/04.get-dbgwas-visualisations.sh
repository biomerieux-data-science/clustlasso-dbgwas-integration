#!/bin/sh


########################################################
# specify input and temp directories + "patterns" file #
########################################################
tmpDir=./TMP 
inputDir=../output/dbgwas
patterns=../output/03.best-models-patterns.txt
outputDir=../output/04.dbgwas-visualizations
########################################################

# prepare "step1" directory #
#---------------------------#
# create output dir
mkdir -p $tmpDir
# link to step 1
cp -R $inputDir/step1 $tmpDir/step1
mv $tmpDir/step1/maf_input.unique_rows_to_all_rows.binary $tmpDir/step1/bugwas_input.unique_rows_to_all_rows.binary
mv $tmpDir/step1/maf_input.unitig_to_pattern.binary $tmpDir/step1/gemma_input.unitig_to_pattern.binary
# empty "strain" file
touch $tmpDir/strains.txt

# create step2 dir #
#------------------#
mkdir $tmpDir/step2

# cp pattern file #
#-----------------#
cp $patterns $tmpDir/step2/patterns.txt

# run step3 #
#-----------#
# specify number of neighbors
nh=3
# call DBGWAS
DBGWAS -strains $tmpDir/strains.txt -output $tmpDir -skip2 -SFF 1000 -nh $nh -nc-db ../input/DBGWAS_merged_ResDB.fasta

# save results #
#--------------#
mv $tmpDir/visualisations $outputDir

# remove temp files #
#-------------------#
rm -R $tmpDir

